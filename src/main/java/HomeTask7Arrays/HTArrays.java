package HomeTask7Arrays;

import java.util.Scanner;



public class HTArrays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введіть кількість елементів в масиві:");
        int n = scanner.nextInt();
        int[] array = new int[n];
        System.out.println("Введіть елементи масиву:");
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        double average = calculateAverage(array);
        System.out.println("Середнє значення масиву:");
        int max = findMax(array);
        System.out.println("Максимальне значення в масиві " + max);
        int min = findMin(array);
        System.out.println("Мінімвльне значення в масиві: " + min);
        System.out.println("Елементи масиву в зворотньому порядку");
        printArrayInReverse(array);
        System.out.println("Введіть число для перевірки наявності в масиві:");
        int numberToCheck = scanner.nextInt();
        boolean isPresent = checkNumberInArray(array, numberToCheck);
        System.out.println("Чмсло " + numberToCheck + (isPresent ? " присутнє" : " відсутнє") + " в масиві.");
    }

    public static double calculateAverage(int[] array) {
        int sum = 0;
        for (int num : array) {
            sum += num;
        }
        return (double) sum / array.length;
    }

    public static int findMax(int[] array) {
        int max = array[0];
        for (int num : array) {
            if (num > max) {
                max = num;
            }
        }
        return max;
    }

    public static int findMin(int[] array) {
        int min = array[0];
        for (int num : array) {
            if (num < min) {
                min = num;
            }
        }
        return min;
    }

    public static void printArrayInReverse(int[] array) {
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static boolean checkNumberInArray(int[] array, int number) {
        for (int num : array) {
            if (num == number) {
                return true;
            }
        }
        return false;

    }
}
