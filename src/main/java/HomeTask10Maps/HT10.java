package HomeTask10Maps;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class HT10 {
    public static void main(String[] args) {
        //ДЗ 10.1. Визначення довжини рядка
        //System.out.println(getLengthOfStrings(new String[]{"a", "bb", "a", "bb"}));
        //System.out.println(getLengthOfStrings(new String[]{"this", "and", "that", "and"}));
        //System.out.println(getLengthOfStrings(new String[]{"code", "code", "code", "bug"}));
        //ДЗ 10.2. Масив в Map
        //System.out.println(createMap(new String[]{"code", "bug"}));
        //System.out.println(createMap(new String[]{"man", "moon", "main"}));
        //System.out.println(createMap(new String[]{"man", "moon", "good", "night"}));
        //ДЗ 10.3. Підрахунок слів
        //System.out.println(countWords(new String[]{"a", "b", "a", "c", "b"}));
        //System.out.println(countWords(new String[]{"c", "b", "a"}));
        //System.out.println(countWords(new String[]{"c", "c", "c", "c"}));
        //ДЗ 10.4. Акцентуємося на роботу зі значеннями
        //System.out.println(mergeStringsByFirstChar(new String[]{"salt", "tea", "soda", "toast"}));
        //System.out.println(mergeStringsByFirstChar(new String[]{"aa", "bb", "cc", "aAA", "cCC", "d"}));
        //System.out.println(mergeStringsByFirstChar(new String[]{}));
        //ДЗ 10.5. Парні рядки
        //System.out.println(buildResultString(new String[]{"a", "b", "a"}));
        //System.out.println(buildResultString(new String[]{"a", "b", "a", "c", "a", "d", "a"}));
        //System.out.println(buildResultString(new String[]{"a", "", "a"}));
        //ДЗ 10.6. Повернути елемент, який зустрічається найчастіше
        //System.out.println(findMostFrequentElement(List.of(3, 1, 2, 2, 1, 2, 3, 3, 3)));
        //ДЗ 10.7. Перевірка на перетин множин
        //LinkedHashSet<Integer> setA = new LinkedHashSet<>();
        //setA.add(1);
        //setA.add(2);
        //setA.add(3);
        //setA.add(4);
        //LinkedHashSet<Integer> setB = new LinkedHashSet<>();
        //setB.add(3);
        //setB.add(5);
        //setB.add(6);
        //System.out.println(hasIntersection(setA, setB));
        //ДЗ 10.8. Об'єднання двох множин
        TreeSet<Integer> setA = new TreeSet<>();
        setA.add(1);
        setA.add(2);
        setA.add(3);
        TreeSet<Integer> setB = new TreeSet<>();
        setB.add(3);
        setB.add(4);
        setB.add(5);
        TreeSet<Integer> resultSet = unionSets(setA, setB);
        System.out.println(resultSet);
    }
    //Задача полягає в тому, щоб створити і повернути Map<String, Integer>,
    // де кожен унікальний рядок з масиву String[] буде ключем, а значенням буде його довжина.
    //getLengthOfStrings(["a", "bb", "a", "bb"]) → {"bb": 2, "a": 1}
    //getLengthOfStrings(["this", "and", "that", "and"]) → {"that": 4, "and": 3, "this": 4}
    //getLengthOfStrings(["code", "code", "code", "bug"]) → {"code": 4, "bug": 3}
    public static Map<String, Integer> getLengthOfStrings(String[] strings) {
        Map<String, Integer> lengthMap = new HashMap<>();
        for (String str : strings) {
            if (!lengthMap.containsKey(str)) {
                lengthMap.put(str, str.length());
            }
        }
        return lengthMap;
    }
    //Завдання полягає в тому, щоб створити і повернути Map<String, String>,
    // де ключами будуть перші символи кожного рядка з непорожнього масиву String[],
    // а значеннями будуть останні символи відповідних рядків.
    //createMap(["code", "bug"]) → {"b": "g", "c": "e"}
    //createMap(["man", "moon", "main"]) → {"m": "n"}
    //createMap(["man", "moon", "good", "night"]) → {"g": "d", "m": "n", "n": "t"}
    public static Map<String, String> createMap(String[] strings) {
        Map<String, String> resultMap = new HashMap<>();
        for (String str : strings) {
            if (!str.isEmpty()) {
                String firstChar = str.substring(0, 1);
                String lastChar = str.substring(str.length() - 1);
                resultMap.put(firstChar, lastChar);
            }
        }
        return resultMap;
    }
    //Це класичний алгоритм підрахунку слів: задано масив рядків, повернути Map<String, Integer>
    // з ключем для кожного різного рядка та значенням - кількістю разів, як цей рядок зустрічається в масиві.
    //countWords(["a", "b", "a", "c", "b"]) → {"a": 2, "b": 2, "c": 1}
    //countWords(["c", "b", "a"]) → {"a": 1, "b": 1, "c": 1}
    //countWords(["c", "c", "c", "c"]) → {"c": 4}
    public static Map<String, Integer> countWords(String[] strings) {
        Map<String, Integer> wordCountMap = new HashMap<>();
        for (String str : strings) {
            if (wordCountMap.containsKey(str)) {
                wordCountMap.put(str, wordCountMap.get(str) + 1);
            }
            else {
                wordCountMap.put(str, 1);
            }
        }
        return wordCountMap;
    }
    //Ця задача передбачає створення і повернення Map<String, String>,
    // де ключами будуть різні перші символи зустрінутих рядків,
    // а значеннями будуть всі рядки, які починаються з цього символу, об'єднані у порядку їх зустрічі у вихідному масиві.
    //mergeStringsByFirstChar(["salt", "tea", "soda", "toast"]) → {"s": "saltsoda", "t": "teatoast"}
    //mergeStringsByFirstChar(["aa", "bb", "cc", "aAA", "cCC", "d"]) → {"a": "aaaAA", "b": "bb", "c": "cccCC", "d": "d"}
    //mergeStringsByFirstChar([]) → {}
    public static Map<String, String> mergeStringsByFirstChar(String[] strings) {
        Map<String, StringBuilder> tempMap = new HashMap<>();
        Map<String, String> resultMap = new HashMap<>();
        for (String str : strings) {
            if (!str.isEmpty()) {
                String firstChar = str.substring(0, 1);
                tempMap.computeIfAbsent(firstChar, k -> new StringBuilder()).append(str);
            }
        }
        for (Map.Entry<String, StringBuilder> entry : tempMap.entrySet()) {
            resultMap.put(entry.getKey(), entry.getValue().toString());
        }

        return resultMap;
    }
    //Якщо рядок зустрічається в масиві вдруге, вчетверте, шосте рази і так далі,
    // додайте цей рядок до результату(тільки перше входження).
    // Якщо ж жодний рядок не зустрічається вдруге, поверніть пустий рядок.
    //buildResultString(["a", "b", "a"]) → "a”
    //buildResultString(["a", "b", "a", "c", "a", "d", "a"]) → "a”
    //buildResultString(["a", "", "a"]) → "a”
    public static String buildResultString(String[] strings) {
        Map<String, Integer> countMap = new HashMap<>();
        StringBuilder result = new StringBuilder();
        for (String str : strings) {
            countMap.put(str, countMap.getOrDefault(str, 0) + 1);
            if (countMap.get(str) % 2 == 0) {
                result.append(str);
            }
        }
        return result.toString();
    }
    //Знайдіть елемент, який зустрічається найчастіше у списку.
    //Приклад: Для списку [3, 1, 2, 2, 1, 2, 3, 3, 3], очікуваний результат 3, оскільки він зустрічається найбільшу кількість разів.
    public static int findMostFrequentElement(List<Integer> list) {
        Map<Integer, Integer> frequencyMap = new HashMap<>();
        for (Integer num : list) {
            frequencyMap.put(num, frequencyMap.getOrDefault(num, 0) + 1);
        }
        int mostFrequentElement = list.get(0);
        int maxFrequency = 0;
        for (Map.Entry<Integer, Integer> entry : frequencyMap.entrySet()) {
            if (entry.getValue() > maxFrequency) {
                mostFrequentElement = entry.getKey();
                maxFrequency = entry.getValue();
            }
        }
        return mostFrequentElement;
    }
    //Приклад: Для LinkedHashSet A = [1, 2, 3, 4] та LinkedHashSet B = [3, 5, 6], очікуваний результат true.
    public static boolean hasIntersection(LinkedHashSet<Integer> setA, LinkedHashSet<Integer> setB) {
        Set<Integer> copyOfSetA = new LinkedHashSet<>(setA);
        copyOfSetA.retainAll(setB);
        return !copyOfSetA.isEmpty();
    }
    // Об'єднайте два TreeSet у третій TreeSet, зберігаючи унікальність елементів.
    //Приклад: Для TreeSet A = [1, 2, 3] та TreeSet B = [3, 4, 5], очікуваний результат [1, 2, 3, 4, 5].
    public static TreeSet<Integer> unionSets(TreeSet<Integer> setA, TreeSet<Integer> setB) {
        TreeSet<Integer> resultSet = new TreeSet<>(setA);
        resultSet.addAll(setB);
        return resultSet;
    }


}
