package HomeTask6String;

public class HTString {
    public static void main(String[] args) {
        System.out.println(xyzThere("abcxyz"));
        System.out.println(xyzThere("abc.xyz"));
        System.out.println(xyzThere("xyz.abc"));
        //System.out.println(zipZap("zipXzap"));
        //System.out.println(zipZap("zopzop"));
        //System.out.println(zipZap("zzzopzop"));
        //System.out.println(xyzMiddle("AAxyzBB"));
        //System.out.println(xyzMiddle("AxyzBB"));
        //System.out.println(xyzMiddle("AxyzBBB"));
        //System.out.println(mixString("abc", "xyz"));
        //System.out.println(mixString("Hi", "There"));
        //System.out.println(mixString("xxxx", "There"));
        //System.out.println(repeatEnd("Hello", 3));  // llollollo
        //System.out.println(repeatEnd("Hello", 2));  // lolo
        //System.out.println(repeatEnd("Hello", 1));  // o

    }
    //ДЗ 6.1. Рядок, створений з n повторень останніх n символів
    public static  String repeatEnd(String str, int n) {
        String end  = str.substring(str.length() - n);
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < n; i++) {
            result.append(end);
        }
        return result.toString();
    }
    //ДЗ 6.2. Заміксуємо рядки
    public static String mixString(String a, String b) {
        StringBuilder result = new StringBuilder();
        int maxLength = Math.max(a.length(), b.length());
        for (int i = 0; i < maxLength; i++) {
            if (i < a.length()) {
                result.append(a.charAt(i));
            }
            if (i <b.length()) {
                result.append(b.charAt(i));
            }
        }
        return result.toString();
    }
    //ДЗ 6.3. Xyz у середині рядка
    public static boolean xyzMiddle(String str) {
        int len = str.length();
        int xyzPos = str.indexOf("xyz");
        while (xyzPos != -1) {
            int leftLen = xyzPos;
            int rightLen = len - (xyzPos + 3);
            if (Math.abs(leftLen - rightLen) <= 1) {
                return true;
            }
            xyzPos = str.indexOf("xyz", xyzPos + 1);
        }
        return false;
    }
    //ДЗ 6.4. ZipZap
    public static String zipZap(String str) {
        StringBuilder result = new StringBuilder();
        int i = 0;
        while (i < str.length()) {
            if (i <= str.length() - 3 && str.charAt(i) == 'z' && str.charAt(i + 2) == 'p') {
              result.append("zp");
              i += 3;
            }
            else {
                result.append(str.charAt(i));
                i++;
            }
        }
        return result.toString();
    }
    //ДЗ 6.5. Xyz
    public static boolean xyzThere(String str) {
        for (int i = 0; i <= str.length() - 3; i++) {
            if (str.substring(i, i + 3).equals("xyz")) {
                if (i == 0 || str.charAt(i - 1) != '.') {
                    return true;
                }
            }
        }
        return false;
    }


}
