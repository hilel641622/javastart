package HomeTask11Oop;
import java.util.*;

public class HT11 {
    public static void main(String[] args) {
        Person man = new Man("Oleh", 36, Profession.STUDENT);
        Person woman = new Woman("Victoria", 28, Profession.TEAMLEAD);
        Person person = new Man("Artem", 40, Profession.CEO);

        man.displayInfo();
        woman.displayInfo();
        person.displayInfo();

        // Змінюємо професію
        person.changeProfession(Profession.OTHER);
        System.out.println("Updated information:");
        person.displayInfo();
    }

}
