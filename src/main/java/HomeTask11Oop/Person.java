package HomeTask11Oop;

enum Profession {
    STUDENT,
    TEACHER,
    DOCTOR,
    ENGINEER,
    ARTIST,
    CEO,
    TEAMLEAD,
    QA,
    OTHER

}
interface Displayable {
    void displayInfo();
}

abstract class Person implements Displayable {
    private String name;
    private int age;
    private Profession profession;

    public Person(String name, int age, Profession profession) {
        this.name = name;
        this.age = age;
        this.profession = profession;
    }

    public abstract String getGender();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Profession getProfession() {
        return profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    @Override
    public void displayInfo() {
        System.out.println("Name: " + name + ", Age: " + age + ", Profession: " + profession);
    }
    public void displayInfo(boolean showGender) {
        if (showGender) {
            System.out.println("Gender: " + getGender() + ", Name: " + name + ", Age: " + age + ", Profession: " + profession);
        } else {
            displayInfo();
        }
    }

    public void changeProfession(Profession newProfession) {
        this.profession = newProfession;
    }
}