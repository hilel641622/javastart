package HomeTask11Oop;

class Man extends Person {
    public static final String GENDER = "Male";

    public Man(String name, int age, Profession profession) {
        super(name, age, profession);
    }
    @Override
    public String getGender() {
        return GENDER;
    }

    @Override
    public void displayInfo() {
        System.out.println("Gender: " + GENDER + ", Name: " + getName() + ", Age: " + getAge() + ", Profession: " + getProfession());
    }

}
