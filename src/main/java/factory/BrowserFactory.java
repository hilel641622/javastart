package factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {
    public static WebDriver getDriver(String browser) {
        WebDriver driver = null;
        switch (browser.toLowerCase()) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "/home/it_palko/IdeaProjects/javastart/src/main/resources/chromedriver"); // Заміни на актуальний шлях
                driver = new ChromeDriver();
                break;
            case "firefox":
                System.setProperty("webdriver.gecko.driver", "/home/it_palko/IdeaProjects/javastart/src/main/resources/geckodriver"); // Заміни на актуальний шлях
                driver = new FirefoxDriver();
                break;
            default:
                throw new IllegalArgumentException("Browser " + browser + " not supported.");
        }

        return driver;
    }
}