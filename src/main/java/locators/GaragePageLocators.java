package locators;

import org.openqa.selenium.By;

public class GaragePageLocators {
    public static final By ADD_CAR_BUTTON = By.xpath("//button[contains(text(), 'Add car')]");
    public static final By BRAND_DROPDOWN = By.id("addCarBrand");
    public static final By MODEL_DROPDOWN = By.id("addCarModel");
    public static final By MILLAGE_INPUT = By.id("addCarMileage");

    public static final By ADD_CAR_MODAL_BUTTON = By.xpath("//button[@type='button' and contains(@class, 'btn btn-primary')]");

    public static final By ADDED_CAR_NAME = By.xpath("//p[@class='car_name h2' and text()='Audi TT']");

    public static final By CAR_IMAGE = By.xpath("//div[@class='car_logo car-logo']/img[@class='car-logo_img']");

    public static final By MILEAGE_INPUT_FIELD = By.xpath("//input[@name='miles']");

    public static final By CAR_UPDATE_MILEAGE = By.xpath("//p[contains(@class, 'car_update-mileage')]");
    public static final By INSTRUCTIONS_LINK = By.linkText("Instructions");
    // Локатор для кнопки вибору автомобіля
    public static final By CAR_SELECTION_BUTTON = By.xpath("//button[@id='brandSelectDropdown']");
    // Локатор для випадаючого меню з автомобілями
    public static final By CAR_LIST_DROPDOWN = By.xpath("//ul[@class='brand-select-dropdown_menu dropdown-menu show']");


    // Локатор для елементів автомобілів у випадаючому списку
    public static final By CAR_LIST_ITEMS = By.xpath("//ul[@class='brand-select-dropdown_menu dropdown-menu show']//li");


    // Локатор для завантаження файлу
    public static final By DOWNLOAD_LINK = By.xpath("//a[@class='instruction-link_download' and contains(@href, 'instructions/audi/tt/Front windshield wipers on Audi TT.pdf')]");
}
