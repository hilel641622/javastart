package locators;

import org.openqa.selenium.By;

public class LoginPageLocators {
    public static final By GUEST_LOGIN_BUTTON = By.xpath("//button[contains(text(), 'Guest log in')]");
}
