package HomeTask9Collections;
import java.util.*;

public class HT9 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>(Arrays.asList(3, 14, 15, 92, 6));
        System.out.println("Колекція чмсел: " + numbers);

        List<String> names = new ArrayList<>(Arrays.asList("Олег", "Євгенович", "Палко"));
        System.out.println("Колекція імен: " + names);

        Set<String> uniqueElements = new HashSet<>(Arrays.asList("елемент1", "елемент2", "елемент3", "елемент1"));
        System.out.println("Сет унікальних елементів " + uniqueElements);

        Map<String, Integer> cityCodes = new HashMap<>();
        cityCodes.put("Київ", 44);
        cityCodes.put("Одеса", 48);
        cityCodes.put("Хмельницький", 382);
        cityCodes.put("Львів", 32);
        cityCodes.put("Вінниця", 432);
        cityCodes.put("Дніпро", 56);
        System.out.println("Мапа міст та їх телефонних кодів " + cityCodes);
    }
}
