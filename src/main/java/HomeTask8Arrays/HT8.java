package HomeTask8Arrays;

public class HT8 {
    public static void main(String[] args) {
        //ДЗ 8.1. CatDog
        //System.out.println(catDog("catdog"));
        //System.out.println(catDog("catcat"));
        //System.out.println(catDog("1cat1cadodog"));
        //ДЗ 8.1. Повернути кількість парних цілих чисел у заданому масиві
        //System.out.println(countEvenInts(new  int[]{2, 1, 2, 3, 4}));
        //System.out.println(countEvenInts(new  int[]{2, 2, 0}));
        //System.out.println(countEvenInts(new  int[]{1, 3, 5}));
        //ДЗ 8.2. Повернути "центроване" середнє значення масиву цілих чисел
        //System.out.println(centeredAverage(new int[]{1, 2, 3, 4, 100}));
        //System.out.println(centeredAverage(new int[]{1, 1, 5, 5, 10, 8, 7}));
        //System.out.println(centeredAverage(new int[]{-10, -4, -2, -4, -2, 0}));
        //ДЗ 8.3. Повернути суму чисел у масиві оминаючи ділянки між 6 та 7
        //System.out.println(sumIgnoreSections(new int[]{1, 2, 2}));
        //System.out.println(sumIgnoreSections(new int[]{1, 2, 2, 6, 99, 99, 7}));
        //System.out.println(sumIgnoreSections(new int[]{1, 1, 6, 7, 2}));
        //ДЗ 8.4. Повернути суму чисел у масиві
        //System.out.println(sumWithoutUnlucky13(new int[]{1, 2, 2, 1}));
        //System.out.println(sumWithoutUnlucky13(new int[]{1, 1}));
        //System.out.println(sumWithoutUnlucky13(new int[]{1, 2, 2, 1, 13}));
        //ДЗ 8.5. Різниця між найбільшим і найменшим значеннями у масиві
        //System.out.println(differenceLargestSmallest(new int[]{10, 3, 5, 6}));
        //System.out.println(differenceLargestSmallest(new int[]{7, 2, 10, 9}));
        //System.out.println(differenceLargestSmallest(new int[]{2, 10, 7, 2}));
        //ДЗ 8.6. Повернути рядок, де кожен елемент дублюється
        //System.out.println(doubleChars("The"));
        //System.out.println(doubleChars("AAbb"));
        //System.out.println(doubleChars("Hi-There"));
        //ДЗ 8.7. Підрахувати кількость входжень підрядка "hi"
        //System.out.println(countHi("abc hi ho"));
        //System.out.println(countHi("ABChi hi"));
        //System.out.println(countHi("hihi"));
        //ДЗ 8.8. Code
        //System.out.println(countCode("aaacodebbb"));
        //System.out.println(countCode("codexxcode"));
        //System.out.println(countCode("cozexxcope\""));
        //ДЗ 8.9. Порівняння двох рядків
        System.out.println(endWith("AbC", "HiaBc"));
        System.out.println(endWith("abc", "abXabc"));
        System.out.println(endWith("Hiabc", "abc"));
    }
    //ДЗ 8.1. CatDog
//Ця задача полягає в порівнянні кількості входжень підрядків "cat" та "dog" у вказаному рядку.
// Якщо обидва підрядки зустрічаються у рядку однакову кількість разів, повертається значення true, в іншому випадку - false.
//catDog("catdog") → true
//catDog("catcat") → false
//catDog("1cat1cadodog") → true
    public static boolean catDog(String str) {
        int catCount = 0;
        int dogCount = 0;
        for (int i = 0; i <= str.length() - 3; i++) {
            if (str.substring(i, i + 3).equals("cat")) {
                catCount++;
            }
        }
        for (int i = 0; i <= str.length() - 3; i++) {
            if (str.substring(i, i + 3).equals("dog")) {
                dogCount++;
            }
        }
        return catCount == dogCount;
    }
    //ДЗ 8.1. Повернути кількість парних цілих чисел у заданому масиві
    //Повернути кількість парних цілих чисел у заданому масиві.
    // Зверніть увагу: оператор % "mod" обчислює залишок від ділення, наприклад, 5 % 2 дорівнює 1.
    //countEvenInts([2, 1, 2, 3, 4]) → 3
    //countEvenInts([2, 2, 0]) → 3
    //countEvenInts([1, 3, 5]) → 0
    public static int countEvenInts(int[] nums) {
        int count = 0;
        for (int num : nums) {
            if (num % 2 == 0) {
                count++;
            }
        }
        return count;
    }
    //ДЗ 8.2. Повернути "центроване" середнє значення масиву цілих чисел
    //Повернути "центроване" середнє значення масиву цілих чисел, яке є середнім значенням елементів,
    // за винятком найбільшого та найменшого значень у масиві. Якщо є кілька копій найменшого значення,
    // ігнорувати лише одну копію, так само із найбільшим значенням.
    // Використовуйте цілочисельне ділення для обчислення кінцевого середнього.
    // Можна припускати, що довжина масиву дорівнює 3 або більше.
    //centeredAverage([1, 2, 3, 4, 100]) → 3
    //centeredAverage([1, 1, 5, 5, 10, 8, 7]) → 5
    //centeredAverage([-10, -4, -2, -4, -2, 0]) → -3
    public static int centeredAverage(int[] nums) {
        int sum = 0;
        int min = nums[0];
        int max = nums[0];
        for (int num : nums) {
            sum += num;
            if (num < min) {
                min = num;
            }
            if (num > max) {
                max = num;
            }
        }
        sum = sum - min - max;
        return  sum / (nums.length - 2);
    }
    //ДЗ 8.3. Повернути суму чисел у масиві оминаючи ділянки між 6 та 7
    //Повернути суму чисел у масиві, ігноруючи ділянки чисел, що починаються з 6 і закінчуються на наступному 7
    // (кожен 6 буде наслідувати принаймні одна 7). Повернути 0 у випадку відсутності чисел.
    //
    //sumIgnoreSections([1, 2, 2]) → 5
    //sumIgnoreSections([1, 2, 2, 6, 99, 99, 7]) → 5
    //sumIgnoreSections([1, 1, 6, 7, 2]) → 4
    public  static int sumIgnoreSections(int[] nums) {
        int sum = 0;
        boolean ignore = false;
        for (int num : nums) {
            if (num == 6) {
                ignore = true;
            } else if (ignore && num == 7) {
                ignore = false;

            } else if (!ignore) {
                sum += num;
            }
        }
        return sum;
    }
    //ДЗ 8.4. Повернути суму чисел у масиві
    //Повернути суму чисел у масиві, повертаючи 0 для порожнього масиву. Однак число 13 - дуже нещасливе, тому воно не враховується,
    // а також числа, які йдуть одразу після числа 13, також не враховуються.
    //sumWithoutUnlucky13([1, 2, 2, 1]) → 6
    //sumWithoutUnlucky13([1, 1]) → 2
    //sumWithoutUnlucky13([1, 2, 2, 1, 13]) → 6
    public static int sumWithoutUnlucky13(int[] nums) {
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 13) {
                i++;
            }
            else {
                sum += nums[i];
            }
        }
        return sum;
    }
    //ДЗ 8.5. Різниця між найбільшим і найменшим значеннями у масиві
    //масив довжиною 1 або більше цілих чисел. Повернути різницю між найбільшим і найменшим значеннями у масиві.
    // Зверніть увагу: вбудовані методи Math.min(v1, v2) та Math.max(v1, v2) повертають менше або більше з двох значень.
    //differenceLargestSmallest([10, 3, 5, 6]) → 7
    //differenceLargestSmallest([7, 2, 10, 9]) → 8
    //differenceLargestSmallest([2, 10, 7, 2]) → 8
    public static int differenceLargestSmallest (int[] nums) {
        int min = nums[0];
        int max = nums[0];
        for (int i = 1; i < nums.length; i++) {
            min = Math.min(min, nums[i]);
            max = Math.max(max, nums[i]);
        }
        return max - min;
    }
    //ДЗ 8.6. Повернути рядок, де кожен елемент дублюється
    //Дано рядок. Потрібно повернути рядок, де кожен символ з оригінального рядка повторюється двічі.
    //doubleChars("The") → "TThhee”
    //doubleChars("AAbb") → "AAAAbbbb”
    //doubleChars("Hi-There") → "HHii--TThheerree”
    public static String doubleChars(String str) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            result.append(c).append(c);
        }
        return result.toString();
    }
    //ДЗ 8.7. Підрахувати кількость входжень підрядка "hi"
    //Задача полягає в підрахунку кількості входжень підрядка "hi" у вказаному рядку.
    //countHi("abc hi ho") → 1
    //countHi("ABChi hi") → 2
    //countHi("hihi") → 2
    public static int countHi(String str) {
        int count = 0;
        for (int i = 0; i <= str.length() - 2; i++) {
            if (str.substring(i, i + 2).equals("hi")) {
                count++;
            }
        }
        return count;
    }
    //ДЗ 8.8. Code
    //Задача полягає в підрахунку кількості входжень підрядка "code" у вказаному рядку,
    // при цьому приймаються будь-які літери для символу 'd'.
    // Отже, рядки "cope" і "cooe" також вважаються входженням "code".
    //countCode("aaacodebbb") → 1
    //countCode("codexxcode") → 2
    //countCode("cozexxcope") → 2
    public static int countCode(String str) {
        int count = 0;
        for (int i = 0; i <= str.length() - 4; i++) {
            if (str.substring(i, i + 2).equals("co") && str.charAt(i + 3) == 'e') {
                count++;
            }
        }
        return count;
    }
    //ДЗ 8.9. Порівняння двох рядків
    //Задача передбачає порівняння двох рядків та визначення, чи один рядок з'являється в кінці іншого рядка,
    // ігноруючи різницю у великих і малих літерах (тобто порівняння має бути "case insensitive" або нечутливим до регістру).
    // Функція поверне true, якщо хоча б один з рядків з'являється в кінці іншого.
    //endsWith("AbC", "HiaBc") → true
    //endsWith("abc", "abXabc") → true
    //endsWith("Hiabc", "abc") → true
    public static boolean endWith(String str1, String str2) {
        String lowerStr1 = str1.toLowerCase();
        String lowerStr2 = str2.toLowerCase();
        return lowerStr1.endsWith(lowerStr2) || lowerStr2.endsWith(lowerStr1);
    }
}
