package pages;

import locators.LoginPageLocators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
    private final WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }
    public void openPage(String url) {
        driver.get(url);
    }

    public void clickGuestLoginButton() {
        WebElement guestLoginButton = driver.findElement(LoginPageLocators.GUEST_LOGIN_BUTTON);
        guestLoginButton.click();
    }
}


