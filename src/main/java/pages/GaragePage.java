package pages;

import locators.GaragePageLocators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
import org.openqa.selenium.WebElement;
import java.util.List;
import java.util.stream.Collectors;
import java.time.Duration;

public class GaragePage {
    private final WebDriver driver;
    private WebDriverWait wait;


    public GaragePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
    }
    public void clickAddCarButton() {
        driver.findElement(GaragePageLocators.ADD_CAR_BUTTON).click();
    }

    public void selectBrand(String brand) {
        driver.findElement(GaragePageLocators.BRAND_DROPDOWN).sendKeys(brand);
    }

    public void selectModel(String model) {
        driver.findElement(GaragePageLocators.MODEL_DROPDOWN).sendKeys(model);
    }

    public void enterMillage(String millage) {
        driver.findElement(GaragePageLocators.MILLAGE_INPUT).sendKeys(millage);
    }

    public void clickAddCarModalButton() {
        driver.findElement(GaragePageLocators.ADD_CAR_MODAL_BUTTON).click();
    }
    public void clickInstructions() {
        WebElement instructionsLink = driver.findElement(GaragePageLocators.INSTRUCTIONS_LINK);
        instructionsLink.click();
    }

    public void downloadInstructionFile(String s) {
        WebElement fileLink = driver.findElement(GaragePageLocators.DOWNLOAD_LINK);
        fileLink.click();
    }
    // Метод для кліку на кнопку вибору автомобіля
    public void clickCarSelectionButton() {
        WebElement carSelectionButton = driver.findElement(GaragePageLocators.CAR_SELECTION_BUTTON);
        carSelectionButton.click();

        // Очікуємо, поки випадаюче меню з'явиться після кліку
        wait.until(ExpectedConditions.visibilityOfElementLocated(GaragePageLocators.CAR_LIST_DROPDOWN));
    }

    // Метод для отримання списку доступних автомобілів
    public List<WebElement> getCarListElements() {
        return driver.findElements(GaragePageLocators.CAR_LIST_ITEMS);
    }

    public boolean isCarAdded() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(GaragePageLocators.ADDED_CAR_NAME));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isCarImageDisplayed() {
        return driver.findElement(GaragePageLocators.CAR_IMAGE).isDisplayed();
    }

    public String getCarImageSrc() {
        return driver.findElement(GaragePageLocators.CAR_IMAGE).getAttribute("src");
    }

    public String getMileage() {
        return driver.findElement(GaragePageLocators.MILEAGE_INPUT_FIELD).getAttribute("value");
    }

    public String getUpdateMileageDate() {
        return driver.findElement(GaragePageLocators.CAR_UPDATE_MILEAGE).getText();
    }
    public List<String> getCarList() {
        WebElement carSelectionButton = driver.findElement(GaragePageLocators.CAR_SELECTION_BUTTON);
        carSelectionButton.click();

        List<WebElement> cars = driver.findElements(GaragePageLocators.CAR_LIST_ITEMS);
        return cars.stream().map(WebElement::getText).collect(Collectors.toList());
    }
}