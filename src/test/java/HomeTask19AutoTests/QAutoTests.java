package HomeTask19AutoTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.openqa.selenium.support.Color;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class QAutoTests {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/home/it_palko/IdeaProjects/javastart/src/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test(description = "Перевірка відображення логотипу", priority = 1)
    public void testLogoDisplayed() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

        WebElement logo = driver.findElement(By.xpath("//*[contains(@class, 'header_logo')]"));

       // if (logo.isDisplayed()) {
       //     System.out.println("Logo displayed");
       // } else {
       //     System.out.println("Logo does not displayed");
        Assert.assertTrue(logo.isDisplayed(), "Logo does not displayed");

        System.out.println("Logo is displayed successfully.");
    }

    @Test(description = "Перевірка кольору фону кнопки 'Sign up'", priority = 2)
    public void testSignUpButtonBackgroundColor() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

        WebElement signUpButton = driver.findElement(By.xpath("//button[text()='Sign up']"));

        String backgroundColor = signUpButton.getCssValue("background-color");

        String expectedColor = "#0275d8";

        String actualColor = Color.fromString(backgroundColor).asHex();

       // if (expectedColor.equals(actualColor)) {
       //     System.out.println("Background color of Sign up button is correct");
       // } else {
       //     System.out.println("Background color of Sign up button is incorrect");
        Assert.assertEquals(actualColor, expectedColor, "Background color of Sign up button is incorrect");

        System.out.println("Background color of Sign up button is correct.");
        }
    }


