package HomeTask21;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Set;

public class QAutoSocialMediaTests {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "/home/it_palko/IdeaProjects/javastart/src/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Test(description = "Перевірка тайтлу у фреймі з використанням JavascriptExecutor")
    public void testFrameTitleWithJS() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

        WebElement frame = driver.findElement(By.tagName("iframe"));
        driver.switchTo().frame(frame);

        JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
        String actualTitle = (String) jsExecutor.executeScript("return document.title;");

        System.out.println("Actual Title: " + actualTitle);

        String expectedTitle = "Як потрапити у майбутнє? Трансформація навчання. - YouTube";

        Assert.assertEquals(actualTitle, expectedTitle, "Title doesn’t equals to the expected result");

        driver.switchTo().defaultContent();
    }

    @Test(description = "Перевірка блока соціальних мереж у футері сайту")
    public void testSocialMediaBlock() {
        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

        WebElement socialMediaBlock = driver.findElement(By.cssSelector("div.contacts_socials.socials"));
        List<WebElement> socialIcons = socialMediaBlock.findElements(By.cssSelector("a.socials_link"));

        Assert.assertEquals(socialIcons.size(), 5, "Social network block doesn’t contain 5 items");

        String[] expectedUrls = {
                "https://www.facebook.com/Hillel.IT.School",
                "https://t.me/ithillel_kyiv",
                "https://www.youtube.com/user/HillelITSchool?sub_confirmation=1",
                "https://www.instagram.com/hillel_itschool/",
                "https://www.linkedin.com/school/ithillel/"
        };

        for (int i = 0; i < socialIcons.size(); i++) {
            WebElement icon = socialIcons.get(i);
            String originalWindow = driver.getWindowHandle();
            Set<String> existingWindows = driver.getWindowHandles();

            icon.click();

            String newWindow = waitForNewWindow(driver, existingWindows);
            Assert.assertNotNull(newWindow, "New tab did not open after clicking");

            driver.switchTo().window(newWindow);
            String actualUrl = driver.getCurrentUrl();
            Assert.assertEquals(actualUrl, expectedUrls[i], "Incorrect URL for social network");

            driver.close();
            driver.switchTo().window(originalWindow);
        }
    }

    private String waitForNewWindow(WebDriver driver, Set<String> existingWindows) {
        for (int i = 0; i < 10; i++) {
            Set<String> allWindows = driver.getWindowHandles();
            allWindows.removeAll(existingWindows);
            if (allWindows.size() > 0) {
                return allWindows.iterator().next();
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}

