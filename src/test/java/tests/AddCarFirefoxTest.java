package tests;

import factory.BrowserFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.GaragePage;
import pages.LoginPage;

import java.time.Duration;

public class AddCarFirefoxTest {
    private WebDriver driver;
    private GaragePage garagePage;

    @BeforeMethod
    public void setUp() {
        driver = BrowserFactory.getDriver("firefox");

        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickGuestLoginButton();

        garagePage = new GaragePage(driver);
    }

    @Test
    public void testAddCarFirefox() {
        garagePage.clickAddCarButton();
        garagePage.selectBrand("Audi");
        garagePage.selectModel("TT");
        garagePage.enterMillage("20");
        garagePage.clickAddCarModalButton();

        Assert.assertTrue(garagePage.isCarAdded(), "Car was not added.");
        Assert.assertTrue(garagePage.isCarImageDisplayed(), "Car image is not displayed.");
        Assert.assertTrue(garagePage.getCarImageSrc().endsWith("audi.png"), "Car image source is incorrect.");
        Assert.assertEquals(garagePage.getMileage(), "20", "Mileage is incorrect.");
        String updateMileageText = garagePage.getUpdateMileageDate();
        Assert.assertTrue(updateMileageText.contains("Update mileage"), "Update mileage text is incorrect.");
        Assert.assertTrue(updateMileageText.contains("08.09.2024"), "Date in 'Update mileage' is incorrect.");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}