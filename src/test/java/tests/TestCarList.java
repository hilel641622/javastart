package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;
import pages.LoginPage;
import pages.GaragePage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.assertTrue;

public class TestCarList {
    private WebDriver driver;
    private final String downloadFolder = "/home/it_palko/IdeaProjects/javastart/src/main/resources";
    private final String outputFilePath = downloadFolder + "/car_list.txt";

    @BeforeMethod
    public void setUp() {
        // Налаштування для завантаження файлів у конкретну папку
        Map<String, Object> pref = new HashMap<>();
        pref.put("download.default_directory", downloadFolder);
        pref.put("download.prompt_for_download", false);
        pref.put("profile.default_content_settings.popups", 0);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", pref);

        // Створюємо новий екземпляр драйвера Chrome
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void testSaveCarList() throws IOException {
        // Логін до системи
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openPage("https://guest:welcome2qauto@qauto.forstudy.space/");
        loginPage.clickGuestLoginButton();

        // Перевірка, що ми на сторінці Garage
        String currentUrl = driver.getCurrentUrl();
        assertTrue(currentUrl.contains("garage"), "Не вдалося перейти на сторінку Garage");



        // Клік на кнопку вибору автомобіля
        GaragePage garagePage = new GaragePage(driver);
        garagePage.clickInstructions();
        garagePage.clickCarSelectionButton();

        // Отримати список доступних автомобілів з випадаючого меню
        List<String> carList = getCarListFromDropdown();

        // Записати список автомобілів у файл
        saveCarListToFile(carList);

        // Перевірка, чи файл було створено
        assertTrue(new File(outputFilePath).exists(), "Файл зі списком автомобілів не був створений");
    }

    private List<String> getCarListFromDropdown() {
        List<String> carList = new ArrayList<>();
        // Локатор для елементів випадаючого списку автомобілів
        List<WebElement> carElements = driver.findElements(By.cssSelector("li.brand-select-dropdown_item"));

        for (WebElement carElement : carElements) {
            carList.add(carElement.getText());
        }
        return carList;
    }

    private void saveCarListToFile(List<String> carList) throws IOException {
        // Запис списку автомобілів у файл
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFilePath))) {
            for (String car : carList) {
                writer.write(car);
                writer.newLine();
            }
        }
    }

    @AfterMethod
    public void tearDown() {
        // Закриваємо браузер після виконання тесту
        if (driver != null) {
            driver.quit();
        }
    }
}