package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;
import pages.LoginPage;
import pages.GaragePage;
import java.io.File;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import static org.testng.Assert.assertTrue;


public class TestDownloadFile {
    private WebDriver driver;
    private final String downloadFolder = "/home/it_palko/IdeaProjects/javastart/src/main/resources";

    @BeforeMethod
    public void setUp() {
        Map<String, Object> pref = new HashMap<>();
        pref.put("download.default_directory", downloadFolder);
        pref.put("download.prompt_for_download", false);
        pref.put("profile.default_content_settings.popups", 0);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", pref);

        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @Test
    public void testDownloadFile() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openPage("https://guest:welcome2qauto@qauto.forstudy.space/");
        loginPage.clickGuestLoginButton();

        // Перевірка переходу на сторінку Garage
        String currentUrl = driver.getCurrentUrl();
        assertTrue(currentUrl.contains("garage"));

        GaragePage garagePage = new GaragePage(driver);
        garagePage.clickInstructions();

        // Завантаження файлу
        garagePage.downloadInstructionFile("Front windshield wipers on Audi TT.pdf");  // Якщо метод без параметрів

        // Перевірка, чи файл завантажився
        assertTrue(isFileDownloaded(downloadFolder + "/Front windshield wipers on Audi TT.pdf", 30), "Файл не був завантажений");
    }

    public boolean isFileDownloaded(String filePath, int timeoutInSeconds) throws InterruptedException {
        File file = new File(filePath);
        int waited = 0;
        while (waited < timeoutInSeconds) {
            if (file.exists()) {
                return true;
            }
            Thread.sleep(1000);  // Чекати 1 секунду
            waited++;
        }
        return false;
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}