import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

public class QautoForstudySpace {
    public static void main(String[] args) {
        //System.setProperty("webdriver.chrome.driver", "/home/it_palko/IdeaProjects/javastart/src/main/resources/chromedriver");



        WebDriver driver = new ChromeDriver();

        try {
            driver.manage().window().maximize();

            driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");

            WebElement guestLoginButton = driver.findElement(By.xpath("//button[text()='Guest log in']"));
            guestLoginButton.click();

            Thread.sleep(5000);

            List<WebElement> borderMenuItems = driver.findElements(By.xpath("//*[contains(@class, 'sidebar-wrapper')]"));

            if (borderMenuItems.isEmpty()) {
                System.out.println("Menu items not found.");
            } else {
                for (WebElement item : borderMenuItems) {
                    System.out.println(item.getText());
                }
            }

        } catch (Exception e) {
            System.err.println("Test failed: " + e.getMessage());
            e.printStackTrace();
        } finally {
            driver.quit();
        }
    }
}