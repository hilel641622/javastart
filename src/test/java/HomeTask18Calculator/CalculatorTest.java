package HomeTask18Calculator;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CalculatorTest {
    private Calculator calculator;

    @BeforeClass
    public void setUp() {
        calculator = new Calculator();
    }

    @AfterClass
    public void tearDown() {
        calculator = null;
    }

    @Test(description = "Test addition method", priority = 1)
    public void testAddition() {
        int result = calculator.add(3, 15);
        System.out.println("Addition result: " + result);
        assert result == 18 : "Addition test failed!";
    }

    @Test(description = "Test subtraction method", priority = 2)
    public void testSubtraction() {
        int result = calculator.subtract(10, 3);
        System.out.println("Subtraction result: " + result);
        assert result == 7 : "Subtraction test failed!";
    }

    @Test(description = "Test multiplication method", priority = 3)
    public void testMultiplication() {
        int result = calculator.multiply(5, 6);
        System.out.println("Multiplication result: " + result);
        assert result == 30 : "Multiplication test failed!";
    }

    @Test(description = "Test division method", priority = 4)
    public void testDivision() {
        int result = calculator.divide(9, 3);
        System.out.println("Division result: " + result);
        assert result == 3 : "Division test failed!";
    }

    @Test(description = "Test division by zero", expectedExceptions = ArithmeticException.class, priority = 5)
    public void testDivisionByZero() {
        calculator.divide(9, 0);
    }
}
