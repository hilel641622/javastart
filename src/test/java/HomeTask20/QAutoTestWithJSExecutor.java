package HomeTask20;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class QAutoTestWithJSExecutor {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "/home/it_palko/IdeaProjects/javastart/src/chromedriver");

        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();

        try {
            driver.get("https://guest:welcome2qauto@qauto.forstudy.space/");
            System.out.println("Page loaded successfully.");

            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.titleIs("Hillel Qauto"));
            System.out.println("Page title is 'Hillel Qauto'.");

            JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
            WebElement guestLoginButton = driver.findElement(By.xpath("//button[text()='Guest log in']"));
            jsExecutor.executeScript("arguments[0].click();", guestLoginButton);
            System.out.println("Clicked on 'Guest log in' button using JavascriptExecutor.");

            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Add car']")));
            System.out.println("Button 'Add car' is now clickable.");

        } catch (Exception e) {
            System.err.println("Test failed: " + e.getMessage());
            e.printStackTrace();
        } finally {
            driver.quit();
        }
    }
}